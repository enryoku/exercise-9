﻿//Corrected by Mark W Paynter
//use for IC08
//Lydia's code
// In this situation I had to test where the code was going and how it was failing. I used many Console.WriteLine() statements to hunt down where the program was going.
// I also had to use the ReadKey() in order to keep the console window open so I could see what was happening.
// I chose to make multiple spelling and best practice changes to the code so I could understand it better. Then I discovered the list.union functionality and found out how to fix the program.
// This was very challenging for me. I definitely do not expect full credit, but any points you feel that I deserve would be appreciated.
// Thank you for the challenge.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST117_IC08_console
{
    class Program
    {
        static void Main(string[] args)
        {
            //make some sets
            Set A = new Set();
            Set B = new Set();
            //add new set to contain union values
            Set C = new Set();

            //put some stuff in the sets
            Random r = new Random();
            for (int i = 0; i < 10; i++)
            {
                //   A.addElement(r.Next(4));
                A.AddElement(r.Next(4));
                //   B.addElement(r.Next(12));
                B.AddElement(r.Next(12));
            }

            //display each set and the union
            Console.WriteLine("A: " + A);
            Console.WriteLine("B: " + B);
            foreach (int item in C.Union(A, B))
            {
                C.AddElement(item);
            }
            // Console.WriteLine("A union B: " + A.union(B));
            Console.WriteLine("A union B: " + C);

            //display original sets (should be unchanged)
            Console.WriteLine("After union operation");
            Console.WriteLine("A: " + A);
            Console.WriteLine("B: " + B);
            // keep the window open
            Console.WriteLine("Press any key to continue . . .");
            Console.ReadKey();
        }
    }
}