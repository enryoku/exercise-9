﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Lydia's code - find the errors!
namespace CST117_IC08_console
{
    class Set
    {
        private List<int> elements;


        public Set()
        {
            elements = new List<int>();
        }

        //public bool addElement(int val)
        public bool AddElement(int val)
        {
            //if (containsElement(val)) return false;
            //return false;
            if (ContainsElement(val))
            {
                return false;
            }
            else
            {
                elements.Add(val);
                return true;
            }
        }

        //private bool containsElement(int val)
        private bool ContainsElement(int val)
        {
            // add return variable to set true/false
            //bool returnValue = false;
            //if (elements.Count == 0)
            //{
            //    return returnValue;
            //}
            //else
            //{
            //for (int i = 0; i < elements.Count; i++)
            //for (int i = 0; i <= elements.Count; i++)
            //foreach (int i in elements)
            //{
            //   if (val == elements[i])
            //       return true;
            //   else
            //       return false;
            //if (val == elements[i])
            //if (val == i)
            //{
            //    returnValue = true;
            //}
            //else
            //{
            //    returnValue = false;
            //}
            //}
            //return returnValue;
            bool returnValue = false;
            foreach (int item in elements)
            {
                if (val == item)
                {
                    returnValue = true;
                    break;
                }
            }
            return returnValue;
            }

        public override string ToString()
        {
            string str = "";
            foreach (int i in elements)
            {
                str += i + " ";
            }
            return str;
        }

        //public void clearSet()
        public void ClearSet()
        {
            elements.Clear();
        }

        //public Set union(Set rhs)
        public List<int> Union(Set lhs, Set rhs)
        {

            //for (int i = 0; i < rhs.elements.Count; i++)
            //{
            //this.addElement(rhs.elements[i]);
            //this.AddElement(rhs.elements[i]);
            //}
            //return rhs;
            List<int> returnList = new List<int>();
            returnList = lhs.elements.Union(rhs.elements).ToList();
            return returnList;
        }
    }
}